/**
 * Created with IntelliJ IDEA.
 * User: minyee
 * Date: 12/1/12
 * Time: 3:14 PM
 * To change this template use File | Settings | File Templates.
 */

// Load the application once the DOM is ready, using `jQuery.ready`:
$(function(){

    // Book Model
    // ----------
    // Our basic **Book** model has `title` and `page` attributes.
    var Book = Backbone.Model.extend({
        // Default attributes for the book item.
        defaults: function() {
            return {
                title: "empty book...",
                page: false
            };
        }

    });

    // Book Collection, is this specifically for local storage?
    // ---------------

    // The collection of books is backed by *localStorage* instead of a remote
    // server.
    var BookList = Backbone.Collection.extend({

        // Reference to this collection's model.
        model: Book,

        // Save all of the book items under the `"books-backbone"` namespace.
        localStorage: new Backbone.LocalStorage("books-backbone")

    });

    // Create our global collection of **Books**.
    var Books = new BookList;

    // Book Item View
    // --------------
    // The DOM element for a book item...
    var BookView = Backbone.View.extend({

        //... is a list tag.
        // tagName:  "li",

        // Cache the template function for a single item.
        template: _.template($('#item-template').html()),

        // The DOM events specific to an item.
        events: {
            "dblclick .view"  : "edit",
            "click a.destroy" : "clear",
            "keypress .edit"  : "updateOnEnter",
            "keypress .edit-page"  : "updateOnEnter",
            "click .edit-button" : "edit"
        },

        // The BookView listens for changes to its model, re-rendering. Since there's
        // a one-to-one correspondence between a **Book** and a **BookView** in this
        // app, we set a direct reference on the model for convenience.
        initialize: function() {
            this.model.on('change', this.render, this);
            this.model.on('destroy', this.remove, this);
        },

        // Re-render the titles of the book item.
        render: function() {
            this.$el.html(this.template(this.model.toJSON()));
            this.input = this.$('.edit');
            this.pageContent = this.$('.edit-page');
            return this;
        },

        // Switch this view into `"editing"` mode, displaying the input field.
        edit: function() {

            // Hide all other edit inputs first
            $("div").find("#title").css("display","inline");
            $("div").find("#page").css("display","inline");
            $("div").find(".edit").css("display","none");
            $("div").find(".edit-page").css("display","none");

            this.$el.find("#title").css("display","none"); // To refactor later
            this.$el.find("#page").css("display","none");
            // Enable edit mode
            this.$el.find(".edit").css("display","inline");
            this.$el.find(".edit-page").css("display","inline");
        },

        // Close the `"editing"` mode, saving changes to the book.
        close: function() {
            var value = this.input.val();
            var value2 = this.pageContent.val();

            if (!value) {
                this.clear();
            } else {
                this.model.save({title: value, page: value2});
                this.$el.find("#title").css("display","inline"); // To refactor later
                this.$el.find("#page").css("display","inline");
                // Enable edit mode
                this.$el.find(".edit").css("display","none");
                this.$el.find(".edit-page").css("display","none");
            }
        },

        // If you hit `enter`, we're through editing the item.
        updateOnEnter: function(e) {
            if (e.keyCode == 13) this.close();
        },

        // Remove the item, destroy the model.
        clear: function() {
            this.model.destroy();
        }

    });

    // The Application
    // ---------------

    // Our overall **AppView** is the top-level piece of UI.
    var AppView = Backbone.View.extend({

        // Instead of generating a new element, bind to the existing skeleton of
        // the App already present in the HTML.
        el: $("#bookapp"),

        // Our template for the line of statistics at the bottom of the app.
        // statsTemplate: _.template($('#stats-template').html()),

        // Delegated events for creating new items, and clearing completed ones.
        events: {
            "keypress #new-book":  "createOnEnter",
            "click #add-button" : "updateOnClick"
        },

        // At initialization we bind to the relevant events on the `Books`
        // collection, when items are added or changed. Kick things off by
        // loading any preexisting books that might be saved in *localStorage*.
        initialize: function() {

            this.input = this.$("#new-book");

            Books.on('add', this.addOne, this);
            Books.on('reset', this.addAll, this);
            Books.on('all', this.render, this);

            this.main = $('#main');

            Books.fetch();

        },

        // Re-rendering the App just means refreshing the statistics -- the rest
        // of the app doesn't change.
        render: function() {

            if (Books.length) {
                this.main.show();
            }

        },

        // Add a single book item to the list by creating a view for it, and
        // appending its element to the `<ul>`.
        addOne: function(book) {
            var view = new BookView({model: book});
            this.$("#book-list").append(view.render().el);
        },

        // Add all items in the **Books** collection at once.
        addAll: function() {
            Books.each(this.addOne);
        },

        // If you hit return in the main input field, create new **Books** model,
        // persisting it to *localStorage*.
        createOnEnter: function(e) {
            if (e.keyCode != 13) return;
            if (!this.input.val()) return;

            // Hide all other edit inputs first
            $("div").find("#title").css("display","inline"); // dup code
            $("div").find("#page").css("display","inline");
            $("div").find(".edit").css("display","none");
            $("div").find(".edit-page").css("display","none");

            var pageContent = this.$('#new-page').val();

            Books.create({title: this.input.val(), page: pageContent});
            this.input.val('');
            this.$('#new-page').val('');

        },

        updateOnClick: function(e) {

            if (e.target.id != "add-button") return;
            if (!this.input.val()) return;

            // Hide all other edit inputs first
            $("div").find("#title").css("display","inline"); // dup code
            $("div").find("#page").css("display","inline");
            $("div").find(".edit").css("display","none");
            $("div").find(".edit-page").css("display","none");

            var pageContent = this.$('#new-page').val();

            Books.create({title: this.input.val(), page: pageContent});
            this.input.val('');
            this.$('#new-page').val('');

        }

    });

    // Finally, we kick things off by creating the **App**.
    var App = new AppView;

});